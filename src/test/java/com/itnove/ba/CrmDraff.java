package com.itnove.crm.CRM;


import com.itnove.ba.BaseTest;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static junit.framework.TestCase.assertTrue;


/**
 * Unit test for simple App.
 */
public class CrmDraff extends BaseTest {

    @Test
    public void testApp()  throws InterruptedException {
        // empiezo con el website, para dejar el basetest que pueda servir para todos los proyectos
        driver.navigate().to("http://crm.votarem.lu");
        Thread.sleep(2000);

        //acción de loguearse
        Actions hover = new Actions(driver);
        WebElement register = driver.findElement(By.xpath(".//*[@id='user_name']"));
        hover.moveToElement(register).build().perform();
        register.sendKeys("user");
        Thread.sleep(2000);
        WebElement password = driver.findElement(By.xpath(".//*[@id='user_password']"));
        hover.moveToElement(password).build().perform();
        password.sendKeys("bitnami");
        WebElement intro = driver.findElement(By.xpath(".//*[@id='bigbutton']"));
        hover.moveToElement(intro).build().perform();
        intro.click();
        Thread.sleep(3000);

        //acciòn de buscar
        // quiero definir ua int llamado termino que se determine Ripoll
        WebElement boton = driver.findElement(By.xpath(".//*[@id='ajaxHeader']/nav/div/div[5]/ul"));
        boton.click();
        Thread.sleep(3000);
        WebElement busca = driver.findElement(By.xpath(".//*[@id='ajaxHeader']/nav/div/div[5]/ul/li[2]/div[1]/form"));
        hover.moveToElement(busca).build().perform();
        busca.click();
        busca.sendKeys("Ripoll");
        busca.click();
        Thread.sleep(3000);
        WebElement entra = driver.findElement(By.xpath(".//*[@id='ajaxHeader']/nav/div/div[5]/ul/li[2]/div/form/div/span/button"));
        entra.click();
        Thread.sleep(5000);

       //Para hacer create o otras funciones
        WebElement botonera = driver.findElement(By.xpath(".//*[@id='ajaxHeader']/nav/div/div[5]/ul/li[1]"));
        botonera.click();
        Thread.sleep(3000);
        WebElement oprime = driver.findElement(By.xpath(".//*[@id='ajaxHeader']/nav/div/div[5]/ul/li[1]/ul/li[1]/a"));
        oprime.click();
        Thread.sleep(3000);
        WebElement Nombre = driver.findElement(By.xpath(".//*[@id='name']"));
        Nombre.sendKeys("Mediatic2");
        WebElement NombreClick = driver.findElement(By.xpath(".//*[@id='SAVE']"));
        NombreClick.click();
        Thread.sleep(3000);

//Lista
        List<WebElement> menu=driver.findElements(By.xpath(".//*[@id='toolbar']/ul/li"));
        System.out.println(menu.size());
        //List<WebElement> botones;
        WebElement botone = driver.findElement(By.xpath(".//*[@id='toolbar']/ul/li[1]"));
        hover.moveToElement(botone).build().perform();
        botone.click();
        for (int i=0; i < menu.size() -1; i++){
            WebElement botones = driver.findElement(By.xpath(".//*[@id='toolbar']/ul/li[" + (i+1) + "]"));
            hover.moveToElement(botones).build().perform();
            if (i > 2);
            List<WebElement> listBotones = driver.findElements(By.xpath(".//*[@id='toolbar']/ul/li[8]/span/ul/li"));
            System.out.println(listBotones.size());
        }


        // logout
        WebElement ninot = driver.findElement(By.xpath(".//*[@id='ajaxHeader']/nav/div/div[5]/ul/li[5]"));
        hover.moveToElement(ninot).build().perform();
        ninot.click();
        Thread.sleep(5000);
        WebElement fuera = driver.findElement(By.xpath(".//*[@id='ajaxHeader']/nav/div/div[5]/ul/li[5]/ul/li[6]/a"));
        fuera.click();
        Thread.sleep(8000);

    }
}