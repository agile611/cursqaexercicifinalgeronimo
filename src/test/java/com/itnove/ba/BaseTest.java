package com.itnove.ba;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
<<<<<<< HEAD
import org.junit.After;
import org.junit.Before;

=======
>>>>>>> 5afaf92363996da5779ae7a6e5a895973ea01e88

import java.io.File;
import java.io.IOException;


/**
 * Created by guillem on 29/02/16.
 */
public class BaseTest {
    public RemoteWebDriver driver;
    public WebDriverWait wait;
    public Actions hover;
  //  public int timeOut = 10;

    @Before
    public void setUp() throws IOException {
        String browser = System.getProperty("browser");
        if (browser != null && browser.equalsIgnoreCase("firefox")) {
            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            System.setProperty("webdriver.chrome.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "chromedriver-linux");
            driver = new ChromeDriver(capabilities);
        } else {
            DesiredCapabilities capabilities = DesiredCapabilities.firefox();
            System.setProperty("webdriver.gecko.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "geckodriver-linux");
            driver = new FirefoxDriver(capabilities);
        }
       //wait = new WebDriverWait(driver, timeOut);
       hover = new Actions(driver);
        driver.manage().deleteAllCookies();
        driver.manage().window().fullscreen();
       // driver.manage().timeouts().pageLoadTimeout(timeOut, TimeUnit.SECONDS);
       // driver.manage().timeouts().setScriptTimeout(timeOut, TimeUnit.SECONDS);
       // driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
        //Accedir a pagina
      //  driver.navigate().to("http:/crm.votarem.lu");
    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
